import Vue from "vue"
import Router from "vue-router"
Vue.use(Router)

import login from "./components/login.vue"
import register from "./components/register.vue"
import register2 from "./components/register2.vue"

import dashboard from "./users/pages/dashboard.vue"
import dictonary from "./users/pages/dictonary.vue"
import dictonary1 from "./users/pages/old_dictonary.vue"
import trainning from "./users/pages/trainning.vue"



const routes = [
    {
        path: "/login",
        component: login,
        name: '/login'
    },
    {
        path: "/register",
        component: register,
        name: '/register'
    },
    {
        path: "/register2",
        component: register2,
        name: '/register2'
    },

    {
        path: "/",
        component: dashboard,
        name: '/'
    },
    {
        path: "/dictonary",
        component: dictonary,
        name: '/dictonary'
    },
    {
        path: "/trainning",
        component: trainning,
        name: '/trainning'
    },

]

export default new Router({
    mode: "history",
    routes
})
