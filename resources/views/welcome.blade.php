<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Dictonary</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        
        {{-- Scripts  --}}
        <script>
        
            (function () {
                window.Laravel = {
                    csrfToken: '{{csrf_token() }}'
                };
            })();
            
            
        </script>
        <style>
            small {
                color: red;
                display: none;
            }
            .mx_form_inv .mx_empty_filed ~ small {
                display: block;
            }
            .mx_form_inv .mx_recaptcha_failed small {
                display: block;
            }
            .passwords{
                display:none
            }
        </style>
        <!-- <script src="https://www.google.com/recaptcha/api.js" async defer></script> -->
    </head>
    <body>
    
        <div id="app">
        @if(Auth::check())
            @if(Auth::user()->role_id == 1)
                <mainapp  :user="{{Auth::user()}}"></mainapp>
            @else

            @endif
        @else
                <login_register></login_register>    
        @endif
        </div>

        <script src="{{asset('js/app.js')}}"></script>
        <!-- <script src="https://unpkg.com/vue-recaptcha@latest/dist/vue-recaptcha.js"></script> -->
        <!-- Minify -->
        <script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit" async defer></script> 
       <!-- <script src="https://unpkg.com/vue-recaptcha@latest/dist/vue-recaptcha.min.js"></script> -->
        
    </body>
</html>
