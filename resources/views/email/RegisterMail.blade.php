<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TestMail</title>

        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    </head>

    <body>
        <h1>
            Hello
            {{$details['name']}}
        </h1>
        <p>
        Your verify Code : <br><br>
            {{$details['code']}} 
        </p>
        <p>
            
        </p>
    </body>