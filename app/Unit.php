<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable=["id", "user_id", "unit", "word_count", "language1", "language2"];

    public function user(){
        return $this->belongsTo("App\User");
    }

    public function other_unit(){
        return $this->hasOne("App\Other_Unit");
    }

    public function dictonaries(){
        return $this->hasMany("App\Dictonary");
    }

}
