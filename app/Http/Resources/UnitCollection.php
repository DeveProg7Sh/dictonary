<?php

namespace App\Http\Resources;

use App\Other_Unit;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
class UnitCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'Unit' => $this->Unit,
            "word_count" => $this->word_count,
            "unit_permission"=> new Other_UnitCollection($this->other_unit),
            'dictonaries'=> DictonaryCollection::collection($this->dictonaries),
            'language1'=>$this->language1,
            'language2'=>$this->language2,
            'user' => new UserCollection($this->user),
        ];
    }
}
