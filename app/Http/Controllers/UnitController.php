<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unit;
use App\Other_Unit;
use App\Dictonary;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\UnitCollection;

class UnitController extends Controller
{
    public function GetUnits(){

        $tr = Unit::orderBy('created_at')->where("user_id", Auth::user()->id)->get();

        return  UnitCollection::collection($tr);
    }

    public function GetOthersUnits(){
        $per = Other_Unit::orderBy('created_at')->where("users", "0")->get();
        //dd($per[0]->unit_id);
        $tr = [];
        for($i=0;$i<count($per);$i++){
            //print(intval($per[$i]->unit_id));
            $dt = Unit::orderBy('created_at')->where("id", intval($per[$i]->unit_id))->where("user_id", '!=' ,Auth::user()->id)->get();
            //print($dt);
            if( count($dt) != 0 ){
                array_push($tr, UnitCollection::collection($dt));
            }
        }

        return  $tr;
    }

    public function CreateUnit(Request $request){
        $this->validate($request,[
            "Unit"=>"required"
        ]);

        $tek = Unit::where("Unit", $request->Unit)->where('user_id', Auth::user()->id)->first();

        if(!is_null($tek)){
            return response()->json([
                'msg'=> 'A unit with such a name already exists'
            ], 500);
        }

        $unit = Unit::create([
            "unit"=>$request->Unit,
            "user_id"=>Auth::user()->id,
            "word_count"=>0,
            "language1"=>"English",
            "language2"=>"Uzbek"
        ]);
        $users = $request->users ? "0" : "-1";

        Other_unit::create([
            "unit_id"=>$unit->id,
            "users"=>$users,
            "permission"=>"all"
        ]);

        return response()->json([
            'unit'=> $unit
        ], 201);
    }

    public function EditUnit(Request $request){
        $this->validate($request, [
            "Unit"=>"required"
        ]);

        $tek = Unit::where("Unit", $request->Unit)->where('user_id', Auth::user()->id)->where('id', "!=",$request->id)->first();

        if(!is_null($tek)){
            return response()->json([
                'msg'=> 'A unit with such a name already exists'
            ], 500);
        }

        $unit = Unit::where('id', $request->id)->update([
            "unit"=>$request->Unit
        ]);
        $users = $request->users ? "0" : "-1";

        Other_unit::where('unit_id', $request->id)->update([
            "users"=>$users
        ]);

        return response()->json([
            'unit'=> $unit
        ], 200);

    }

    public function DeleteUnit(Request $request){
        $this->validate($request,[
            'id'=>"required"
        ]);


        $oud = Other_Unit::where('unit_id', $request->id)->delete();
        Dictonary::where('unit_id', $request->id)->delete();
        return Unit::where('id', $request->id)->delete();
    }

    public function EditUnitLanguage(Request $request){
        return Unit::where('id', $request->id)->update([
            'language1'=>$request->language1,
            'language2'=>$request->language2
        ]);
    }
}
