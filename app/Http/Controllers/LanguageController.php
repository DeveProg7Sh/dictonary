<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Language;
class LanguageController extends Controller
{
    public function GetLanguages(){

        return Language::orderBy("languages_name")->get();
    }
}
