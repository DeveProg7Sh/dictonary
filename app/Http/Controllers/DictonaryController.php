<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dictonary;
use App\Unit;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DictonaryController extends Controller
{
    public function CreateUnitWords(Request $request){
        $this->validate($request, [
            'unit_id' => 'required'
        ]);
        $msg = [];

        for($i=0;$i<count($request->words);$i++){
            $dict = Dictonary::create([
                'unit_id' => $request->unit_id,
                'user_id' => Auth::user()->id,
                'language1' => $request->words[$i]['language1'],
                'language2' => $request->words[$i]['language2']
            ]);
            array_push($msg, $dict);
        }
        $n_c = DB::select("select unit_id from Dictonaries where unit_id = $request->unit_id");
        Unit::where('id', $request->unit_id)->update(['word_count'=>count($n_c)]);
        return response()->json([
            'msg'=> $msg,
            'word_count' => count($n_c)
        ], 201);
    }

    public function EditUnitWord(Request $request){
        $this->validate($request, [
            'id'=> 'required',
            'language1'=>'required',
            'language2'=>'required'
        ]);

        return Dictonary::where('id', $request->id)->update([
            'language1'=>$request->language1,
            'language2'=>$request->language2,
        ]);
    }

    public function DeleteUnitWord(Request $request){
        Dictonary::where('id', $request->id)->delete();

        $n_c = DB::select("select unit_id from Dictonaries where unit_id = $request->unit_id");
        Unit::where('id', $request->unit_id)->update(['word_count'=>count($n_c)]);
        return response()->json([
            'msg'=> "Succesfully Delete Word",
            'word_count' => count($n_c)
        ], 201);
    }
}
