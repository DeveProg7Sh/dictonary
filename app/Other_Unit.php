<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Other_Unit extends Model
{
    protected $fillable = ['id','users','unit_id','permission'];

    public function user()
        {
            return $this->belongsTo('App\User', 'foreign_key', 'other_key');
        }

    public function unit()
    {
        return $this->belongsTo('App\Unit', 'foreign_key', 'other_key');
    }
}
