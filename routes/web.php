<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\UserCheck;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('user')->middleware([UserCheck::class])->group(function(){

    Route::post('send_email', 'HomeController@send_email');
    Route::post('register_user', 'HomeController@register_user');
    Route::post('login_user', 'HomeController@login_user');

    Route::get('get_units', 'UnitController@GetUnits');
    Route::get('get_other_units', 'UnitController@GetOthersUnits');
    Route::post('edit_unit_language', 'UnitController@EditUnitLanguage');
    Route::post('create_unit', 'UnitController@CreateUnit');
    Route::post('edit_unit', 'UnitController@EditUnit');
    Route::post('delete_unit', 'UnitController@DeleteUnit');

    Route::post('create_unit_words', 'DictonaryController@CreateUnitWords');
    Route::post('edit_unit_word', 'DictonaryController@EditUnitWord');
    Route::post('delete_unit_word', 'DictonaryController@DeleteUnitWord');

    Route::get('get_languages', 'LanguageController@GetLanguages');

});

Route::get('/{any}', "HomeController@index")->where('any', '.*');
